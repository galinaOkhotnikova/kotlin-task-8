import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(3) { i ->
                println("I'm sleeping $i ...")
                delay(1000L)

            }
        } finally {
            println("main: I'm tired of waiting!\n" +
                    "I'm running finally\n" +
                    "main: Now I can quit.")
            delay(2000L)

        }
    }
}
