import kotlinx.coroutines.*
import java.time.*

suspend fun first(): Int {
    delay(1000L)
    return 5
}

suspend fun second(): Int {
    delay(1000L)
    return 7
}

suspend fun main(args: Array<String>) = coroutineScope {
    val clock:Clock=Clock.systemDefaultZone()
    var timeStart = clock.millis()
    println(first() + second())
    var timeEnd = clock.millis()
    println("Time = ${timeEnd -timeStart}")
    timeStart = clock.millis()
    val res1 = async{ first() }
    val res2 = async{ second() }
    println(res1.await() + res2.await())
    timeEnd = clock.millis()
    println("Time = ${timeEnd - timeStart}")
}

/*Можно заметить, что при последовательном вызове функций время выполнения приблизительно в два раза больше.
Это связано с тем, что функции вызываются одна за другой. Когда же мы вызываем их асинхронно, выполнение 
функций происходит параллельно. */
